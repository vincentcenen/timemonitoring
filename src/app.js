const express = require('express'),
      path = require('path'),
      morgan = require('morgan'),
      mysql = require('mysql'),
      myConnection = require('express-myconnection'),
      serveStatic = require('serve-static'),
      nodeDateTime = require('node-datetime')

const app = express();

// importing routes
const employeeRoutes = require('./routes/employee');

// settings
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// middlewares
app.use(morgan('dev'));
app.use(myConnection(mysql, {
  host: 'localhost',
  user: 'root',
  password: 'password',
  port: '',
  database: 'timeAttendance'
}, 'single'));
app.use(express.urlencoded({extended: false}));

// routes
app.use('/', employeeRoutes);

// static files
app.use(express.static(path.join(__dirname, 'public')));
app.use( serveStatic( __dirname + "/assets") )

// starting the server
app.listen(app.get('port'), () => {
  console.log(`server on port ${app.get('port')}`);
});
