const router = require('express').Router();

const employeeController = require('../controllers/employeeController');

// employeeRoutes
router.get('/', employeeController.list);
router.post('/add', employeeController.save);
router.get('/update/:id', employeeController.edit);
router.post('/update/:id', employeeController.update);
router.get('/delete/:id', employeeController.delete);

// dtrRoutes
const dtrController = require('../controllers/dtrController');

router.post('/checkRfid', dtrController.checkRfid);

const testController = require('../controllers/testController')

// testRoutes
router.post('/testing', testController.sampleSave)

const loginController = require('../controllers/loginController')

router.post('/login', loginController.login)


module.exports = router;

