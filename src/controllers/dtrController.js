const controller = {};
var dateTime = require('node-datetime');
var conn = require('./../db.js')

function checkRfidExists(req){
	var rfid = req.body.rfid;
	conn.query("select count(empId) as result from employee where rfid = '"+ rfid +"'", function(err, rows) {
			var result = rows[0].result;
			console.log(result);
			if (result > 0) return true;
		})
	return false;
}

function timeIn(req, res, date){
	var time = dateTime.create().format('H:M:S');
		conn.query("Insert into dtr_table (rfid, timeIn, date) values ('"+req.body.rfid+"', '"+time+"', '"+date+"' )")
		res.json({
			"status": "TIME IN SUCCESS",
			"message" : "Have a good day!"
		})
}

function checkTimeOut (req, res, date){
	req.getConnection((err,conn) => {
		conn.query("select timeOut from dtr_table where rfid = '"+ req.body.rfid +"' and date='"+ date +"' ", function (err, rows) {

			if (rows[0].timeOut == null) {
				timeOut(req, res, date)
			}

			else {
				res.json({
					"status": "ALREADY TIME OUT"
				})
			}	
		})
	})
}

function timeOut(req, res, date){
	var time = dateTime.create().format('H:M:S');
	conn.query("Update dtr_table set timeOut = '"+ time +"' where rfid = '"+ req.body.rfid +"' and date = '"+ date +"'")
	res.json({
		"status" : "TIME OUT SUCCESS",
		"message" : "Goodbye, fella."
	})
}

controller.checkRfid = (req, res) => {
	const date = dateTime.create().format('Y-m-d');
	if(checkRfidExists(req)){
		const rfid = req.body.rfid;
			conn.query("SELECT count (id) as result from dtr_table where rfid= '"+ rfid +"' and date = '"+ date +"'", 
				function(err, rows) {
					var result = rows[0].result;
					if (result == 0) {
						timeIn(req,res, date);
					}

					else {
						checkTimeOut(req, res, date);
					}

			})
	}
	else{
		res.json({
			"message" : "UNKOWN CARD"
		})
	}

};


module.exports = controller;