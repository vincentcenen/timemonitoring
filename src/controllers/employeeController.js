const controller = {};
var conn = require('./../db.js')

controller.list = (req, res) => {
  conn.query('SELECT * FROM employee', (err, employees) => {
   if (err) {
    res.json(err);
   }
   res.render('employees', {
      data: employees
   });
  });
}; 

controller.save = (req, res) => {
  const data = req.body;
  conn.query("SELECT * from employee where rfid='"+ req.body.rfid +"'", (err, rows) => {
      if(err){
        console.log(err);
        res.json({"error" : err})
      }
      else{
        if(rows.length == 0){
          conn.query('INSERT INTO employee set ?', data);
          res.json({
            "status" : "success",
            "message" : "Successfully added"
          })
        }
        else{
          res.json({
            "status" : "Unable to add",
            "message" : "RFID is already present. Tap a new card."
          })
        }
      }
  })
};


controller.edit = (req, res) => {
  const { id } = req.params;
  conn.query("SELECT * FROM employee WHERE id = ?", [id], (err, rows) => {
    res.render('employees_edit', {
      data: rows[0]
    })
  });
};

controller.update = (req, res) => {
  const { id } = req.params;
  const newEmployee = req.body;
  conn.query('UPDATE employee set ? where id = ?', [newEmployee, id], (err, rows) => {
    res.redirect('/');
  });
};

controller.delete = (req, res) => {
  const { id } = req.params;
  conn.query('DELETE FROM employee WHERE id = ?', [id], (err, rows) => {
    res.redirect('/');
  });
}

module.exports = controller;
